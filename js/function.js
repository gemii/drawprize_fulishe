/**
 * Created by walter on 10/26/16.
 */

function dataLoad(filename) {
    var arraydata;
    $.ajax({
        type: "GET",
        url: filename,
        dataType: "json",
        async: false,
        success: function (json) {
            arraydata = eval(json)
        }
    });
    return arraydata;
}
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return (r[2]);
    return null;
}
function checkMobile(str) {
    var re = /^1\d{10}$/;
    if (re.test(str)) {
        return true;
    } else {
        alert("您输入的电话号码格式不正确!");
        return false;
    }
}

function prizeLeve(state) {
    $(".wrap").fadeIn();
    const leveOne = "<div class='inner'>" +
        "<div class='innerHeader'>恭喜宝妈获得金牌大奖</div>" +
        "<div class='LeveOneTag'></div>" +
        "<div class='innerFooter'>恭喜宝妈获得价值198元的:<br>立舒宝消毒液一瓶。<br>我们将在3个工作日内安排发货,<br>请注意查收!</div>" +
        "</div>" +
        "<div class='cancelButt'></div>";
    const leveThree = "<div class='inner'>" +
        "<div class='innerHeader'>恭喜宝妈获得幸运大奖</div>" +
        "<div class='LeveThreeTag'></div>" +
        "<div class='innerFooter'>恭喜宝妈获得巧虎乖乖勺一只。<br>我们将在3个工作日内安排发货,<br>请注意查收!</div>" +
        "</div>" +
        "<div class='cancelButt'></div>";
    const leveTwo = "<div class='inner'>" +
        "<div class='innerHeader'>恭喜宝妈获得幸运大奖</div>" +
        "<div class='LeveTwoTag'></div>" +
        "<div class='innerFooter'>恭喜宝妈获得价值60元的:<br>皇室摇铃一件。<br>我们将在3个工作日内安排发货,<br>请注意查收!</div>" +
        "</div>" +
        "<div class='cancelButt'></div>";
    const leveFour = "<div class='inner'>" +
        "<div class='innerHeader'>真遗憾</div>" +
        "<div class='LeveFourTag'></div>" +
        "<div class='innerFooter' id='innerFooter'>感谢宝妈对福栗社的支持!<br>关注栗子妈妈公号,<br>更多福利等着您~</div>" +
        "</div>" +
        "<div class='cancelButt'></div>";
    switch (state){
        case 1:
            $(".wrap").html("");
            $(".wrap").html(leveOne);
            break;
        case 2:
            $(".wrap").html("");
            $(".wrap").html(leveTwo);
            break;
        case 3:
            $(".wrap").html("");
            $(".wrap").html(leveThree);
            break;
        case 4:
            $(".wrap").html("");
            $(".wrap").html(leveFour);
            break;
    }
    $(".cancelButt").get(0).addEventListener("touchstart", function (e) {
        e.preventDefault();
        $(".wrap").fadeOut();
    })
    $("#innerFooter").get(0).addEventListener("touchstart", function (e) {
        e.preventDefault();
        $(".wrap").fadeOut();
        QRcode();
    })
}

function QRcode() {
    $(".QRcode_wrap").fadeIn(0);
    $(".QRcode_wrap").html("");
    $(".QRcode_wrap").html("<div class='QRcode_inner'>" +
        "<img class='QRcode' src='images/QR-code.jpeg' style='width: 100%;padding-top: 220px;'>" +
        "</div>" +
        "<div class='QRcode_cancelButt'></div>");
    $(".QRcode_cancelButt").get(0).addEventListener("touchstart", function (e) {
        e.preventDefault();
        $(".QRcode_wrap").fadeOut(0);
    })
}